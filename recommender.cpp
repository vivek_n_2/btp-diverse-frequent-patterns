//Vivek Nynaru
#include<bits/stdc++.h>
//#include<tr1/unordered_map>

using namespace std;
//using namespace std::tr1;

typedef vector< vector<int> > vec2D;

int transcnt;
int itemcnt;
int minsupport;

struct Item
{
	int name;
	int freq;
};

struct ppcnode
{
	int label;
	int precode;
	int postcode;
	int frequency;
	ppcnode *firstchild;
	ppcnode *nextsibling;
};

struct nlnode
{
	int name;
	int support;
	int bufrow;
	int bufcol;
	int nllength;
	nlnode* firstchild;
	nlnode* nextsibling;
};

struct mergelist
{
	int pre;
	int post;
	int count;
};

bool comp(const Item& lhs,const Item& rhs)
{
	return (lhs.freq>rhs.freq) || ((lhs.freq == rhs.freq) && (lhs.name<rhs.name));
}

Item *freqitems;
int *counter;
int freqcnt = 0;

ppcnode ppcroot;
unordered_map<int,int> sorter;

nlnode* nlroot;

const int mlen = 100000;
void read_data(char *filename, double support)
{
	FILE *in = fopen(filename,"r");
	int i,item;
	int maxit=0;

	// stores count of transactions and items
	transcnt = itemcnt = 0;

	// Allocate memory for counter and initialize to 0
	counter = new int[100000];
	for(int i=0;i<100000;i++)
		counter[i]=0;
	
	// Read input file line by line and count items
	string str;
	while(getline(in, str))
	{
		item = 0;
		if(feof(in))
			break;
		transcnt++;
		for(i=0;i<str.length() && str[i]!='\0' && str[i]!='\n';i++)
		{
			if(str[i]!=' ')
				item = item*10 + str[i]-'0';
			else
			{
				maxit = max(maxit,item);
				if(counter[item]==0)
					itemcnt++;
				counter[item]++;
				item = 0;
			}
		}
	}
	
	// min frequency to become frequent
	minsupport = ceil(support*transcnt);
	cout << "Min count = " << minsupport << endl;
	freqitems = new Item[itemcnt];
	freqcnt = 0;
	for(int i = 0; i <= maxit; i++)
	{
		if(counter[i] >= minsupport)
		{
			freqitems[freqcnt].name = i;
			freqitems[freqcnt++].freq = counter[i];
		}
	}
	// name stores the item name
	// freq stores the count
	sort(freqitems,freqitems+freqcnt, comp);
	// mapping from item name to order of item
	for(int i=0;i<freqcnt;i++)
		sorter[freqitems[i].name]=i;

	cout << "Frequent items count = " << freqcnt << endl;
}

int* headlen;
void build_ppc(char *filename)
{
	FILE *in = fopen(filename,"r");
	int i,item;
	
	// stores the frequent items of transactions
	int tlen;
	Item* transaction = new Item[1000];
	for(i=0;i<1000;i++)
		transaction[i].name = transaction[i].freq = 0;

	int ifreq;
	int created=0;
	// stores number of nodes of type i
	headlen = new int[freqcnt];
	for(i=0;i<freqcnt;i++)
		headlen[i]=0;

	char str[mlen];
	while(fgets(str,mlen,in))
	{
		item = 0,tlen = 0;
		if(feof(in))
			break;
		for(i=0;i<mlen && str[i]!='\0' && str[i]!='\n';i++)
		{
			if(str[i]!=' ')
				item = item*10 + str[i]-'0';
			else
			{
				ifreq = counter[item];
				if(ifreq>=minsupport)
				{
				   transaction[tlen].name = item;
				   transaction[tlen].freq = -sorter[item];
				   tlen++;
				}
				item = 0;
			}
		}

		// Ignore the record if no item is frequent
		if(tlen==0)
			continue;
		// sort items of a transaction in decreasing support
		sort(transaction,transaction+tlen, comp);
		
		// Insert transaction into fp tree
		ppcnode *curnode = &(ppcroot);
		ppcnode *prevnode = NULL;
		int curpos=0;
		// Inserting while nodes are already present in fp tree
		while(curpos<tlen)
		{
			prevnode = NULL;
			ppcnode *child = curnode->firstchild;
			// search if current item is a child of node
			while(child!=NULL)
			{
				if(child->label==transaction[curpos].name)
				{
					child->frequency++;
					curnode=child;
					curpos++;
					break;
				}
				if(child->nextsibling==NULL)
				{
					prevnode = child;
					child = NULL;
					break;
				}
				child = child->nextsibling;
			}
			if(child==NULL)
				break;
		}
		// Inserting new nodes in fp tree
		int curidx;
		for(int j=curpos;j<tlen;j++)
		{
			created++;
			curidx = -transaction[j].freq;
			ppcnode *newnode = new ppcnode;
			newnode->label = transaction[j].name;
			if(prevnode!=NULL)
			{
				prevnode->nextsibling = newnode;
				prevnode=NULL;
			}
			else
				curnode->firstchild = newnode;

			newnode->nextsibling = NULL;
			newnode->firstchild = NULL;
			newnode->frequency=1;
			headlen[curidx]++;
			curnode = newnode;
		}
	}
	cout << "Number of FPnodes = " << created << endl;
}

int** buffer;  // To store ppc codes of nodes in node list tree
int curbufrow; // stores current row of buffer
int curbufcol; // stores current col of buffer
int* bufendcol; // stores last col used in a buffer
pair<int,int>* curbufpos; // stores last used col for node of type i

int preordercount;
int postordercount;

void postorder(ppcnode* node)
{
	if(node->firstchild)
	{
		ppcnode *temp = node->firstchild;
		while(temp!=NULL)
		{
			postorder(temp);
			temp = temp->nextsibling;
		}
	}
	node->postcode = ++postordercount;
}

void preorder(ppcnode* node)
{
	node->precode = preordercount++;
	if(node->label>=0)
	{
		int idx = sorter[node->label];
		int row = curbufpos[idx].first;
		int col = curbufpos[idx].second;
		buffer[row][col] = node->precode;
		buffer[row][col+1] = node->postcode;
		buffer[row][col+2] = node->frequency;
		curbufpos[idx].second+=3;
	}
	if(node->firstchild)
	{
		ppcnode *temp = node->firstchild;
		while(temp!=NULL)
		{
			preorder(temp);
			temp = temp->nextsibling;
		}
	}
}

int bufsize = 100000;
int bufcount = 500000;
void initialize_nltree()
{
	// creating an empty buffer
	buffer = new int*[bufcount];
	bufendcol = new int[bufcount];
	for(int i=0;i<bufcount;i++)
		bufendcol[i]=bufsize;
	curbufpos = new pair<int,int>[freqcnt];
	curbufrow = 0;
	curbufcol = 0;
	buffer[0] = new int[bufsize];
	
	// Initialize empty node
	nlroot = new nlnode;
	nlroot->name = nlroot->support = nlroot->bufrow = nlroot->bufcol = -1;
	nlroot->nllength = 0;
	nlroot->nextsibling = NULL;
	
	// Inserting singleton itemsets into tree in increasing support
	nlnode* tempnl;
	for(int i=freqcnt-1;i>=0;i--) // increasing order of support
	{
		// check if node can be included in this buffer .. if not close this buffer and create new
		if(3*headlen[i]+curbufcol>=bufsize)
		{
			bufendcol[curbufrow] = curbufcol;
			curbufrow+=1;
			buffer[curbufrow] = new int[bufsize];
			curbufcol=0;
		}
		if(i==freqcnt-1)
		{
			nlroot->firstchild = new nlnode;
			tempnl = nlroot->firstchild;
		}
		else
		{
			tempnl->nextsibling = new nlnode;
			tempnl = tempnl->nextsibling;
		}
		tempnl->name = i;
		tempnl->support = freqitems[i].freq;
		tempnl->bufrow = curbufrow;
		tempnl->bufcol = curbufcol;
		tempnl->nllength = headlen[i];
		curbufpos[i] = make_pair(curbufrow,curbufcol);
		curbufcol += 3*headlen[i];
	}
	preordercount = postordercount = 0;
	postorder(&(ppcroot));
	preorder(&(ppcroot));
}

mergelist* mergestore;
int mergeuse;


void generate()
{
	int FreqPatternCount = 0;
	list< nlnode* > queue;
	nlnode* parent = nlroot;
	nlnode* child = nlroot->firstchild;
	while(child!=NULL)
	{
		queue.push_back(child);
		child = child->nextsibling;
	}
	queue.push_back(NULL);
	int s1,row1,col1,lim1;  // size,rowstart,colstart,colend for descendant
	int s2,row2,col2,lim2;  // size,rowstart,colstart,colend for ancestor
	int preptr,postptr;
	int prelim,postlim;
	int presum,postsum;
	int totcount;
	int mergeuse;
	mergestore = new mergelist[100000];
	while(!queue.empty())
	{
		parent = queue.front();
		queue.pop_front();
		if(parent==NULL)
			continue;
		FreqPatternCount++;
		s2 = parent->nllength;
		row2 = parent->bufrow;
		col2 = parent->bufcol;
		lim2 = col2+3*s2;
		nlnode* tempnl; // stores last new node inserted as child to parent
		for(list<nlnode*>::iterator it=queue.begin();(*it)!=NULL;it++)
		{
			s1 = (*it)->nllength;
			row1 = (*it)->bufrow;
			col1 = (*it)->bufcol;
			lim1 = col1+3*s1;
			preptr=postptr=col2;
			postptr++;
			totcount = presum = postsum = mergeuse = 0;
			// Merge (*it) (ancestor) with parent (descendant)
			for(int i=col1;i<lim1;i+=3)
			{
				prelim = buffer[row1][i];
				postlim = buffer[row1][i+1];
				while(preptr<lim2 && buffer[row2][preptr]<=prelim)
				{
					presum += buffer[row2][preptr+2];
					preptr+=3;
				}
				while(postptr<lim2 && buffer[row2][postptr]<postlim)
				{
					postsum += buffer[row2][postptr+1];
					postptr+=3;
				}
				if(postsum>presum)
				{
					mergestore[mergeuse].pre = prelim;
					mergestore[mergeuse].post = postlim;
					mergestore[mergeuse].count = postsum-presum;
					mergeuse++;
					totcount += postsum-presum;
				}
			}
			if(totcount>=minsupport)
			{
				if(3*mergeuse+curbufcol>=bufsize)
				{
					bufendcol[curbufrow] = curbufcol;
					curbufrow+=1;
					buffer[curbufrow] = new int[bufsize];
					curbufcol=0;
				}
				if(parent->firstchild==NULL)
				{
					parent->firstchild = new nlnode;
					tempnl = parent->firstchild;
				}
				else
				{
					tempnl->nextsibling = new nlnode;
					tempnl = tempnl->nextsibling;
				}
				tempnl->name = (*it)->name;
				tempnl->support = totcount;
				tempnl->bufrow = curbufrow;
				tempnl->bufcol = curbufcol;
				tempnl->nllength = mergeuse;
				tempnl->firstchild = tempnl->nextsibling = NULL;
				for(int k=0;k<mergeuse;k++,curbufcol+=3)
				{
					buffer[curbufrow][curbufcol] = mergestore[k].pre;
					buffer[curbufrow][curbufcol+1] = mergestore[k].post;
					buffer[curbufrow][curbufcol+2] = mergestore[k].count;
				}
			}
		}
		tempnl = parent->firstchild;
		while(tempnl!=NULL)
		{
			queue.push_back(tempnl);
			tempnl=tempnl->nextsibling;
		}
		queue.push_back(NULL);
		/*
		if(lim2>=bufendcol[row2])
			delete buffer[row2];
		*/
	}
	cout << "Frequent patterns found = " << FreqPatternCount << endl;
}

int path[1000];
int pathsize=0;

void printpatterns(nlnode *root)
{
	// prints frequent itemsets along with their frequencies
	if(root->name>=0)
	{
		path[pathsize] = root->name;
		pathsize++;
		for(int i=pathsize-1;i>=0;i--)
			cout << freqitems[path[i]].name << " ";
		cout << "(" << root->support << ")\n";
	}
	nlnode *temp = root->firstchild;
	while(temp!=NULL)
	{
		printpatterns(temp);
		temp=temp->nextsibling;
	}
	if(root->name>=0)
		pathsize--;
}

void genkassoc(int cursup)
{
	// prints assoc rules with right side having only one item
	nlnode *temp;
	for(int i=0;i<pathsize;i++)
	{
		temp = nlroot;
		for(int j=0;j<pathsize;j++)
			if(i!=j)
				for(temp=temp->firstchild;temp!=NULL && temp->name!=path[j];temp=temp->nextsibling);
		for(int j=pathsize-1;j>=0;j--)
			if(i!=j)
				cout << freqitems[path[j]].name << " ";
		cout << "=> " << freqitems[path[i]].name << " ";
		cout << "( " << cursup << " / " <<  temp->support << " )\n";
	}
}

void genassocs(nlnode *root)
{
	if(root->name>=0)
	{
		path[pathsize] = root->name;
		pathsize++;
		if(pathsize>1)
			genkassoc(root->support);
	}
	nlnode *temp = root->firstchild;
	while(temp!=NULL)
	{
		genassocs(temp);
		temp=temp->nextsibling;
	}
	if(root->name>=0)
		pathsize--;
}

int* visited;
/*
void dfs()
vec2D connectedcomps(vec2d& adj) {

}
*/

map< pair<int,int> ,int> pairCount;
void initPairCount(nlnode *root) {
	// prints frequent itemsets along with their frequencies
	if(root->name>=0) {
		path[pathsize] = root->name;
		pathsize++;
		if(pathsize==2) {
			pairCount[make_pair(path[1],path[0])] = root->support;
		}
	}
	if(pathsize < 2) {
		nlnode *temp = root->firstchild;
		while(temp!=NULL) {
			initPairCount(temp);
			temp=temp->nextsibling;
		}
	}
	if(root->name>=0)
		pathsize--;
}

vec2D adj;

// u, v are indexes and not the names
bool isEdge(int u, int v,int edgeSup) {
	return pairCount[make_pair(u,v)]>=edgeSup;
}

void buildAdj(vector<int>& nodes, double edgeSupFloat) {
	int edgeSup = ceil(edgeSupFloat*transcnt);
	//edgeSup = 800;
	int nodeCount = nodes.size();
	//cout << "Edging with edgesup " << edgeSup << endl;
	for(int idx = 0;idx < nodeCount;idx++) {
		adj[nodes[idx]].clear();
	}
	for(int idx = 0;idx < nodeCount;idx++) {
		int u = nodes[idx];
		visited[u] = 0;
		for(int idx2 = idx+1;idx2 < nodeCount;idx2++) {
			int v = nodes[idx2];
			if(isEdge(u,v,edgeSup)) {
				adj[u].push_back(v);
				adj[v].push_back(u);
				//cout << freqitems[u].name << " edged with " << freqitems[v].name <<  endl;
			}
		}
	}
}

void connectedComponents(int parent, int compId){
	if(visited[parent])
		return;
	visited[parent] = compId;
	int childCount = adj[parent].size();
	for(int idx=0;idx<childCount;idx++) {
		int child = adj[parent][idx];
		connectedComponents(child, compId);
	}
}

int connectedComponentsUtil(vector<int>& nodes) {
	int nodeCount = nodes.size();
	int compCount = 0;
	for(int idx = 0;idx < nodeCount;idx++) {
		int u = nodes[idx];
		if(!visited[u]) {
			compCount ++;
			connectedComponents(u, compCount);
		}
	}
	return compCount;
}


double EPS = 0.00001;
void recurseCH(vector<int>& nodes) {
	double left = 0;
	double right = 1.0;
	double mid;
	int connectedComps;
	while(left<right && right-left>EPS)
	{
		mid = (left+right)/2.0;
		buildAdj(nodes, mid);
		connectedComps = connectedComponentsUtil(nodes);
		if(connectedComps == 0){
			right = mid;
		}
		else if(connectedComps <= 2){
			left = mid;
		}
		else
			right = mid;
	}
	cout << "MID = " << mid << endl;
	buildAdj(nodes, mid);
	int compCount = connectedComponentsUtil(nodes);
	int* counter = new int[compCount+1];
	for(int idx=0;idx<=compCount;idx++){
		counter[idx] = 0;
	}
	int nodeCount = nodes.size();
	cout << "NodeCount = " << nodeCount << endl;
	for(int idx = 0;idx < nodeCount;idx++) {
		counter[visited[nodes[idx]]]++;
	}
	for(int idx=1;idx<=compCount;idx++){
		cout << counter[idx] << " ";
	}
	cout << endl;
}

void constructCH() {
	visited = (int*) malloc(freqcnt*sizeof(int));
	adj.clear();
	adj.resize(freqcnt);
	for(int i=0;i<freqcnt;i++) {
		visited[i] = 0;
		adj[i].clear();
	}
	pairCount.clear();
	initPairCount(nlroot);
	/*
	for(map< pair<int,int> , int>::iterator it = pairCount.begin();it!=pairCount.end();it++) {
		cout << freqitems[it->first.first].name << " " << freqitems[it->first.second].name << " = " << it->second << endl;
	}
	*/
	vector<int> nodes;
	nodes.clear();
	for(int i=0;i<freqcnt;i++) {
		nodes.push_back(i);
	}
	recurseCH(nodes);
	//connectedcomps
}

int main(int argc,char **argv)
{
	/*
	char filename[] = "../../Datasets/pumsb.dat";
	double support = 0.8;
	*/
	char* filename = argv[1];
	double support = atof(argv[2]);
	// Initialize root of fp tree
	ppcroot.label = -1;
	ppcroot.precode = ppcroot.postcode = ppcroot.frequency = -1;
	ppcroot.firstchild = ppcroot.nextsibling = NULL;
	// Read data and store counts of items
	read_data(filename,support);
	// Build FP tree to store the required part in compressed form
	build_ppc(filename);
	int c=0;
	for(int k=0;k<freqcnt;k++)
		c+=headlen[k];
	cout << "Header count = " << c << endl;
	// create node list tree with sigletons as children of root
	initialize_nltree();
	generate();
	printpatterns(nlroot);
	//cout << "Beginning" << endl;
	//constructCH();
	//genassocs(nlroot);
	return 0;
}
