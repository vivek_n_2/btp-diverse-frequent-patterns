//Vivek Nynaru
#include<bits/stdc++.h>
#include<tr1/unordered_map>

using namespace std;
using namespace std::tr1;

struct Item
{
	int name;
	int freq;
};
struct node
{
    string name;
    vector<struct node*> children;
    vector<int> child;
};


int minsupport;
const int maxusers = 2000;
const int ratingthreshold = 2;
const int recordsthreshold = 300;
void convert_to_format(char* filename)
{
	ifstream infile(filename);
	int j,lim,val,user,movie,rating;
	vector<int> userliked[maxusers];
	for(int i=0;i<maxusers;i++)
		userliked[i].clear();
	int minuser=maxusers,maxuser=0;
	for(string line;getline(infile,line);)
	{
		j=0;
		lim = line.length();	
		for(int i=0;i<3;i++)
		{
			val = 0;
			for(;j<lim && line[j]!='\t';j++)
				val = val*10 + line[j]-'0';
			j++;
			if(i==0)
				user = val;
			else if(i==1)
				movie = val;
			else
				rating = val;
		}
		minuser = min(minuser,user);
		maxuser = max(maxuser,user);
		if(rating>ratingthreshold)
			userliked[user].push_back(movie);
	}
	int mini = 100000,maxi=0;
	ofstream moviefile("movies.txt");
	for(user=minuser;user<=maxuser;user++)
	{
		mini = min(mini,int(userliked[user].size()));
		maxi = max(maxi,int(userliked[user].size()));
		for(vector<int>::iterator it=userliked[user].begin();it<userliked[user].end();it++)
			moviefile << *it << " ";
		moviefile << "\n";
	}
	//cout << mini << " " << maxi << "\n";
}

int *counter;
int transcnt;
int itemcnt;
Item *freqitems;
int freqcnt;
unordered_map<int,int> sorter;
vector< pair<int,int> > transcntsort;

bool comp(const Item& lhs,const Item& rhs)
{
	return (lhs.freq>rhs.freq) || ((lhs.freq == rhs.freq) && (lhs.name<rhs.name));
}

void read_data(double support)
{
	int i,j,lim,item;
	int maxit=0;

	// stores count of transactions and items
	transcnt = itemcnt = 0;

	// Allocate memory for counter and initialize to 0
	counter = new int[100000];
	for(int i=0;i<100000;i++)
		counter[i]=0;
	
	// Read input file line by line and count items
	ifstream infile("movies.txt");
	for(string line;getline(infile,line);)
	{
		j=0;
		lim = line.length();	
		item = 0;
		transcnt++;
		for(i=0;i<lim && line[i]!='\n';i++)
		{
			if(line[i]!=' ')
				item = item*10 + line[i]-'0';
			else
			{
				maxit = max(maxit,item);
				if(counter[item]==0)
					itemcnt++;
				counter[item]++;
				item = 0;
			}
		}
	}
	// min frequency to become frequent
	minsupport = ceil(support*transcnt);
	cout << "Min count = " << minsupport << endl;
	
	freqitems = new Item[itemcnt];
	freqcnt = 0;
	for(int i = 0; i <= maxit; i++)
	{
		if(counter[i] >= minsupport)
		{
			freqitems[freqcnt].name = i;
			freqitems[freqcnt++].freq = counter[i];
		}
	}
	// name stores the item name
	// freq stores the count
	sort(freqitems,freqitems+freqcnt, comp);
	// mapping from item name to order of item
	sorter.clear();
	for(int i=0;i<freqcnt;i++)
		sorter[freqitems[i].name]=i;

	cout << "Frequent items count = " << freqcnt << endl;
}

vector<int> validtid;
void remove_infreq()
{
	int i,j,lim,item,ifreq;
	int mini=1000,maxi=0;
	
	// stores the frequent items of transactions
	int tlen,tid=0;
	Item* transaction = new Item[1000];
	for(i=0;i<1000;i++)
		transaction[i].name = transaction[i].freq = 0;

	ifstream infile("movies.txt");
	ofstream moviefile("moviesnew.txt");
	transcntsort.clear();
	for(string line;getline(infile,line);)
	{
		j=0;
		lim = line.length();	
		item = 0,tlen = 0;
		for(i=0;i<lim && line[i]!='\n';i++)
		{
			if(line[i]!=' ')
				item = item*10 + line[i]-'0';
			else
			{
				ifreq = counter[item];
				if(ifreq>=minsupport)
				{
				   transaction[tlen].name = item;
				   transaction[tlen].freq = -sorter[item];
				   tlen++;
				}
				item = 0;
			}
		}
		transcntsort.push_back(make_pair(tlen,tid));
		tid++;	
		// Ignore the record if no item is frequent
		if(tlen==0)
		{
			moviefile << "\n";
			continue;
		}
		// sort items of a transaction in decreasing support
		sort(transaction,transaction+tlen, comp);
		
		for(i=0;i<tlen;i++)
			moviefile << transaction[i].name << " ";
		moviefile << "\n";
		maxi = max(maxi,tlen);
		mini = min(mini,tlen);
	}
	sort(transcntsort.begin(),transcntsort.end());
	validtid.clear();
	for(i=0;i<recordsthreshold;i++)
		validtid.push_back(transcntsort[i].second);
	sort(validtid.begin(),validtid.end());
	//cout << transcntsort[500].first << "\n"; 
	//cout << mini << " " << maxi << "\n";
	//recordsthreshold = 400;
}

Item superset[1000];
Item subset[100];
int maxidx;
int k=6; // max number of items in a frequent pattern
long long int count2=0;
int height = 4; //Height of concept hierarchy
string parents[1684][4]; //The hierarchy parents-1683 movies and each movie has 4 parents
map<string, int> mapping;
struct node * Hierarchy = (struct node *) malloc(sizeof(struct node));

std::vector<std::string> split(const std::string& s, char c)
{
    std::vector<std::string> v;
    unsigned int i = 0;
    unsigned int j = s.find(c);
    while (j < s.length()) {
        v.push_back(s.substr(i, j - i));
        i = ++j;
        j = s.find(c, j);
        if (j >= s.length()) {
            v.push_back(s.substr(i, s.length()));
            break;
        }
    }
    return v;
}


void insert(char* filename)
{
    std::ifstream input(filename ); //The concept hierarchy
    struct node* temp = Hierarchy;
    temp->name = "root";
    for( std::string line; getline( input, line ); )
    {
        vector<string> words=split(line,'|');
        for(int i=0;i<4;i++)
            parents[atoi(words[4].c_str())][i]=words[i];
        /*for(int i=0;i<words.size();i++)
          cout << words[i] << " ";
          cout << "\n";*/
        struct node* iter=temp;
        int cnt=1;
        while(cnt<4)
        {
            vector<int> v = iter->child; 
            int xyz=find(v.begin(),v.end(),mapping[words[cnt]])-v.begin();
            if (xyz+v.begin()!=v.end())
            {
                //child node exists - do nothing
                iter = iter->children[xyz];
            }
            else
            {
                //create child node
                struct node* cur = new struct node;
                //struct node* cur = (struct node*)calloc(1,sizeof(struct node*));
                cur->name = words[cnt];
                mapping[words[cnt]]=mapping.size();
                iter->child.push_back(mapping[words[cnt]]);
                iter->children.push_back(cur); 
                iter = iter->children[(iter->child).size()-1];
            }
            cnt++;
        }
    }
}

void generate_k_subsets(int curidx, int spaceleft)
{
	if(curidx==maxidx) // exhausted
	{
		if(spaceleft==k)
			return;
		/***************************
		 *  Do the diversity rank on subset, subset[jj].name has the name of item
		 * *************************/
        set<string> calc;
        int length=k-spaceleft;
        float drank;
		for(int jj=0;jj<k-spaceleft;jj++)
        {
            for(int kk=0;kk<height;kk++)
            {
                calc.insert(parents[subset[jj].name][kk]);
            }
			cout << subset[jj].name << " ";
        }
        if(length==1)
            drank=1;
        else
            drank=(calc.size() - (height))/(1.0*(height-1)*(length-1));
        cout << drank << "\n";
		count2++;
		return;
	}
	if(spaceleft==0) // no space
	{
		/***************************
		 *  Do the diversity rank on subset, subset[jj].name has the name of item
		 * *************************/
        set<string> calc;
        int length=k-spaceleft;
        float drank;
		for(int jj=0;jj<k-spaceleft;jj++)
        {
            for(int kk=0;kk<height;kk++)
            {
                calc.insert(parents[subset[jj].name][kk]);
            }
			cout << subset[jj].name << " ";
        }
        if(length==1)
            drank=1;
        else
            drank=1.0*(calc.size() - (height))/((height-1)*(length-1));
        cout << drank << "\n";
		count2++;
		return;
	}
	subset[k-spaceleft] = superset[curidx];
	generate_k_subsets(curidx+1,spaceleft-1);
	generate_k_subsets(curidx+1,spaceleft);
}

void gen_diverse_patterns()
{
	int i,j,lim,item,ifreq;
	
	// stores the frequent items of transactions
	int tlen,tid=0;
	maxidx = 0;
	ifstream infile("moviesnew.txt");
	int okay=0;
	int ptr2 = 0;
	int flag = 0;
	for(string line;getline(infile,line);)
	{
		flag=j=0;
		lim = line.length();	
		item = 0,tlen = 0;
		for(i=0;i<lim && line[i]!='\n';i++)
		{
			if(line[i]!=' ')
				item = item*10 + line[i]-'0';
			else
			{
				ifreq = counter[item];
				if(ifreq>=minsupport)
				{
				   superset[tlen].name = item;
				   superset[tlen].freq = -sorter[item];
				   tlen++;
				}
				item = 0;
			}
		}
		// Ignore the record if no item is frequent
		int idx = lower_bound(validtid.begin(),validtid.end(),tid) - validtid.begin();
		if(ptr2<recordsthreshold && validtid[ptr2] == tid)
		{
			okay++;
			ptr2++;
			flag = 1;
		}
		tid++;	
		if(tlen==0)
			continue;
		// sort items of a transaction in decreasing support
		sort(superset,superset+tlen, comp);
		maxidx = tlen;
		if(flag==1)
		{
			// generate subsets
			generate_k_subsets(0,k);
		}
	}
	cout << count2 << "\n";
}

int main(int argc,char **argv)
{
	if(argc!=4)
	{
		cout << "USAGE: ./a.out <path to hierarchy> <path to moviedata> <support>\n";
		return 0;
	}
	char* infile = argv[2];
    char* hierarchy = argv[1];
    insert(hierarchy);
	double support = atof(argv[3]);
	convert_to_format(infile);
	read_data(support);
	remove_infreq();
	gen_diverse_patterns();
	return 0;
}
