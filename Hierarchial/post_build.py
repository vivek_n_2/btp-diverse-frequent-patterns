from math import sqrt

new_paths = {}
with open("new_hierarchy.dat") as f:
	for i in f.readlines():
		record = i.strip().split('|')
		new_paths[record[-1]] = record


act_paths = {}
with open("act_hierarchy.dat") as f:
	for i in f.readlines():
		record = i.strip().split('|')
		if record[-1] in new_paths:
			act_paths[record[-1]] = record

fin_paths = []
for i in new_paths:
	if i in act_paths:
		fin_paths.append([new_paths[i], act_paths[i]])

tot = len(fin_paths)

def pathlen(A, B, t):
	end = min(len(fin_paths[A][t]), len(fin_paths[B][t]))
	start = 0
	while start < end and fin_paths[A][t][start] == fin_paths[B][t][start]:
		start += 1

	return len(fin_paths[A][t]) + len(fin_paths[B][t]) - 2*start

new_mat = [[0 for j in xrange(tot)] for i in xrange(tot)]
act_mat = [[0 for j in xrange(tot)] for i in xrange(tot)]

for i in xrange(tot):
	for j in xrange(tot):
		new_mat[i][j] = pathlen(i, j, 0)
		act_mat[i][j] = pathlen(i, j, 1)

new_mean = 0
act_mean = 0
den = 0
for i in xrange(tot):
	for j in xrange( i+1, tot):
		new_mean += new_mat[i][j]
		act_mean += act_mat[i][j]
		den += 1

new_mean /= float(den)
act_mean /= float(den)

num_c = 0
den_a = 0
den_b = 0
for i in xrange(tot):
	for j in xrange( i+1, tot):
		num_c += (new_mat[i][j] - new_mean) * (act_mat[i][j] - act_mean)
		den_a += (new_mat[i][j] - new_mean)**2
		den_b += (act_mat[i][j] - act_mean)**2

cophenetic_correlation = abs(num_c) / sqrt(den_a * den_b)
print cophenetic_correlation