act_paths = {}
with open("act_hierarchy.dat") as f:
	for i in f.readlines():
		record = i.strip().split('|')
		act_paths[record[-1]] = record

# for i in act_paths:
# 	print i, act_paths[i]

trns_file = "../Datasets/movies"
virt_trns = trns_file + ".virt"
min_lim = 5

wf = open(virt_trns, "w+")

with open(trns_file) as f:
	for i in f.readlines():
		record = i.strip().split(' ')
		new_record = [movie for movie in record if movie in act_paths]
		# new_record = [movie for movie in record]
		if len(new_record) < min_lim:
			wf.write(' '.join(new_record) + "\n")
			continue
		grps = {}
		for movie in new_record:
			# if movie not in act_paths:
			# 	par = "UNK"
			# else:
			par = act_paths[movie][-2]
			if par not in grps:
				grps[par] = []
			grps[par].append(movie)

		for grp in grps.values():
			wf.write(' '.join(grp) + "\n")

wf.close()

old_length = len(open(trns_file).readlines())
new_length = len(open(virt_trns).readlines())


old_sup = 0.3
new_sup = old_sup*old_length/new_length

print new_sup