new_paths = {}
with open("new_hierarchy.dat") as f:
	for i in f.readlines():
		record = i.strip().split('|')
		new_paths[record[-1]] = record


act_paths = {}
with open("act_hierarchy.dat") as f:
	for i in f.readlines():
		record = i.strip().split('|')
		if record[-1] in new_paths:
			# for j in xrange(len(record)-1):
			# 	record[j] = record[j] + str(j)
			act_paths[record[-1]] = record

name = {}
index = {}
with open("u.item") as f:
	for i in f.readlines():
		record = i.strip().split('|')
		if record[0] in new_paths:
			name[record[0]] = record[1]
			index[record[1]] = record[0]

fin_paths = []
for i in new_paths:
	if i in act_paths:
		fin_paths.append([new_paths[i], act_paths[i]])

tot = len(fin_paths)

adj = {}
for new, act in fin_paths:
	new[-1] = name[new[-1]]
	for i in xrange(len(new) - 1):
		if new[i] not in adj:
			adj[new[i]] = set([])
		adj[new[i]].add(new[i+1])

root = fin_paths[0][0][0]

def commompath(path1, path2):
	if len(path1) == 0:
		return path2
	if len(path2) == 0:
		return path1

	end = min(len(path1), len(path2))
	start = 0
	while start < end and path1[start] == path2[start]:
		start += 1

	return path1[:start]

name_map = {}
cnt_map = {}
def dfs(root):
	if root not in adj:
		return act_paths[index[root]]

	path = []
	for child in adj[root]:
		path = commompath(path, dfs(child))

	if path[-1] not in cnt_map:
		cnt_map[path[-1]] = 0

	cnt_map[path[-1]] += 1
	name_map[root] = path[-1] + str(cnt_map[path[-1]])

	return path

def print_edges(root, f):

	if root not in adj:
		return
	root_name = root
	if root in name_map:
		root_name = name_map[root]


	for child in adj[root]:
		child_name = child
		if child in name_map:
			child_name = name_map[child]
		f.write('"%s" -> "%s"\n' %(root_name, child_name))
		print_edges(child, f)

dfs(root)

f = open("named_hierarchy.dot", 'w+')
f.write('\n'.join( ["digraph G {",
"node [style=filled, color=green]; " + ' '.join(map(lambda x: '"%s"'%(x), name.values())),
"node [style=solid, color=black]"]))

print_edges(root, f)
f.write("}\n")
f.close()


visited = {}
def print_edges_act(root, adj):
	if root in visited:
		return
	
	visited[root] = True
	if root not in adj:
		return

	for child in adj[root]:
		if root != child:
			print '"%s" -> "%s"' %(root, child)
			print_edges_act(child, adj)

adj = {}
for new, act in fin_paths:
	act[-1] = name[act[-1]]
	for i in xrange(len(act) - 1):
		if act[i] not in adj:
			adj[act[i]] = set([])
		adj[act[i]].add(act[i+1])


root = fin_paths[0][1][0]

print "digraph G {"
print "node [style=filled, color=green]; " + ' '.join(map(lambda x: '"%s"'%(x), name.values()))
print "node [style=solid, color=black]"

print_edges_act(root, adj)
print "}"