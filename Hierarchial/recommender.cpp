//Vivek Nynaru
#include<bits/stdc++.h>
//#include<tr1/unordered_map>

using namespace std;
//using namespace std::tr1;

typedef vector< vector<int> > vec2D;

int transcnt;
int itemcnt;
int minsupport;

struct Item
{
	int name;
	int freq;
};

struct ppcnode
{
	int label;
	int precode;
	int postcode;
	int frequency;
	ppcnode *firstchild;
	ppcnode *nextsibling;
};

struct nlnode
{
	int name;
	int support;
	int bufrow;
	int bufcol;
	int nllength;
	nlnode* firstchild;
	nlnode* nextsibling;
};

struct mergelist
{
	int pre;
	int post;
	int count;
};

bool comp(const Item& lhs,const Item& rhs)
{
	return (lhs.freq>rhs.freq) || ((lhs.freq == rhs.freq) && (lhs.name<rhs.name));
}

Item *freqitems;
int *counter;
int freqcnt = 0;

ppcnode ppcroot;
unordered_map<int,int> sorter;

nlnode* nlroot;

void read_data(char *filename, double support)
{
	FILE *in = fopen(filename,"r");
	int i,item;
	int maxit=0;

	// stores count of transactions and items
	transcnt = itemcnt = 0;

	// Allocate memory for counter and initialize to 0
	counter = new int[100000];
	for(int i=0;i<100000;i++)
		counter[i]=0;
	
	// Read input file line by line and count items
	char str[500];
	while(fgets(str,500,in))
	{
		item = 0;
		if(feof(in))
			break;
		transcnt++;
		for(i=0;i<500 && str[i]!='\0' && str[i]!='\n';i++)
		{
			if(str[i]!=' ')
				item = item*10 + str[i]-'0';
			else
			{
				maxit = max(maxit,item);
				if(counter[item]==0)
					itemcnt++;
				counter[item]++;
				item = 0;
			}
		}
	}
	
	// min frequency to become frequent
	minsupport = ceil(support*transcnt);
	cout << transcnt << " " << support << " Min count = " << minsupport << endl;
	freqitems = new Item[itemcnt];
	freqcnt = 0;
	for(int i = 0; i <= maxit; i++)
	{
		if(counter[i] >= minsupport)
		{
			freqitems[freqcnt].name = i;
			freqitems[freqcnt++].freq = counter[i];
		}
	}
	// name stores the item name
	// freq stores the count
	sort(freqitems,freqitems+freqcnt, comp);
	// mapping from item name to order of item
	for(int i=0;i<freqcnt;i++) {
		sorter[freqitems[i].name]=i;
		//cout << "hello " << freqitems[i].name << " " << sorter[freqitems[i].name] << endl;
	}

	cout << "Frequent items count = " << freqcnt << endl;
}

int* headlen;
void build_ppc(char *filename)
{
	FILE *in = fopen(filename,"r");
	int i,item;
	
	// stores the frequent items of transactions
	int tlen;
	Item* transaction = new Item[1000];
	for(i=0;i<1000;i++)
		transaction[i].name = transaction[i].freq = 0;

	int ifreq;
	int created=0;
	// stores number of nodes of type i
	headlen = new int[freqcnt];
	for(i=0;i<freqcnt;i++)
		headlen[i]=0;

	char str[500];
	while(fgets(str,500,in))
	{
		item = 0,tlen = 0;
		if(feof(in))
			break;
		for(i=0;i<500 && str[i]!='\0' && str[i]!='\n';i++)
		{
			if(str[i]!=' ')
				item = item*10 + str[i]-'0';
			else
			{
				ifreq = counter[item];
				if(ifreq>=minsupport)
				{
				   transaction[tlen].name = item;
				   transaction[tlen].freq = -sorter[item];
				   tlen++;
				}
				item = 0;
			}
		}

		// Ignore the record if no item is frequent
		if(tlen==0)
			continue;
		// sort items of a transaction in decreasing support
		sort(transaction,transaction+tlen, comp);
		
		// Insert transaction into fp tree
		ppcnode *curnode = &(ppcroot);
		ppcnode *prevnode = NULL;
		int curpos=0;
		// Inserting while nodes are already present in fp tree
		while(curpos<tlen)
		{
			prevnode = NULL;
			ppcnode *child = curnode->firstchild;
			// search if current item is a child of node
			while(child!=NULL)
			{
				if(child->label==transaction[curpos].name)
				{
					child->frequency++;
					curnode=child;
					curpos++;
					break;
				}
				if(child->nextsibling==NULL)
				{
					prevnode = child;
					child = NULL;
					break;
				}
				child = child->nextsibling;
			}
			if(child==NULL)
				break;
		}
		// Inserting new nodes in fp tree
		int curidx;
		for(int j=curpos;j<tlen;j++)
		{
			created++;
			curidx = -transaction[j].freq;
			ppcnode *newnode = new ppcnode;
			newnode->label = transaction[j].name;
			if(prevnode!=NULL)
			{
				prevnode->nextsibling = newnode;
				prevnode=NULL;
			}
			else
				curnode->firstchild = newnode;

			newnode->nextsibling = NULL;
			newnode->firstchild = NULL;
			newnode->frequency=1;
			headlen[curidx]++;
			curnode = newnode;
		}
	}
	cout << "Number of FPnodes = " << created << endl;
}

int** buffer;  // To store ppc codes of nodes in node list tree
int curbufrow; // stores current row of buffer
int curbufcol; // stores current col of buffer
int* bufendcol; // stores last col used in a buffer
pair<int,int>* curbufpos; // stores last used col for node of type i

int preordercount;
int postordercount;

void postorder(ppcnode* node)
{
	if(node->firstchild)
	{
		ppcnode *temp = node->firstchild;
		while(temp!=NULL)
		{
			postorder(temp);
			temp = temp->nextsibling;
		}
	}
	node->postcode = ++postordercount;
}

void preorder(ppcnode* node)
{
	node->precode = preordercount++;
	if(node->label>=0)
	{
		int idx = sorter[node->label];
		int row = curbufpos[idx].first;
		int col = curbufpos[idx].second;
		buffer[row][col] = node->precode;
		buffer[row][col+1] = node->postcode;
		buffer[row][col+2] = node->frequency;
		curbufpos[idx].second+=3;
	}
	if(node->firstchild)
	{
		ppcnode *temp = node->firstchild;
		while(temp!=NULL)
		{
			preorder(temp);
			temp = temp->nextsibling;
		}
	}
}

int bufsize = 100000;
int bufcount = 500000;
void initialize_nltree()
{
	// creating an empty buffer
	buffer = new int*[bufcount];
	bufendcol = new int[bufcount];
	for(int i=0;i<bufcount;i++)
		bufendcol[i]=bufsize;
	curbufpos = new pair<int,int>[freqcnt];
	curbufrow = 0;
	curbufcol = 0;
	buffer[0] = new int[bufsize];
	
	// Initialize empty node
	nlroot = new nlnode;
	nlroot->name = nlroot->support = nlroot->bufrow = nlroot->bufcol = -1;
	nlroot->nllength = 0;
	nlroot->nextsibling = NULL;
	
	// Inserting singleton itemsets into tree in increasing support
	nlnode* tempnl;
	for(int i=freqcnt-1;i>=0;i--) // increasing order of support
	{
		// check if node can be included in this buffer .. if not close this buffer and create new
		if(3*headlen[i]+curbufcol>=bufsize)
		{
			bufendcol[curbufrow] = curbufcol;
			curbufrow+=1;
			buffer[curbufrow] = new int[bufsize];
			curbufcol=0;
		}
		if(i==freqcnt-1)
		{
			nlroot->firstchild = new nlnode;
			tempnl = nlroot->firstchild;
		}
		else
		{
			tempnl->nextsibling = new nlnode;
			tempnl = tempnl->nextsibling;
		}
		tempnl->name = i;
		tempnl->support = freqitems[i].freq;
		tempnl->bufrow = curbufrow;
		tempnl->bufcol = curbufcol;
		tempnl->nllength = headlen[i];
		curbufpos[i] = make_pair(curbufrow,curbufcol);
		curbufcol += 3*headlen[i];
	}
	preordercount = postordercount = 0;
	postorder(&(ppcroot));
	preorder(&(ppcroot));
}

mergelist* mergestore;
int mergeuse;


void generate()
{
	int FreqPatternCount = 0;
	list< nlnode* > queue;
	nlnode* parent = nlroot;
	nlnode* child = nlroot->firstchild;
	while(child!=NULL)
	{
		queue.push_back(child);
		child = child->nextsibling;
	}
	queue.push_back(NULL);
	int s1,row1,col1,lim1;  // size,rowstart,colstart,colend for descendant
	int s2,row2,col2,lim2;  // size,rowstart,colstart,colend for ancestor
	int preptr,postptr;
	int prelim,postlim;
	int presum,postsum;
	int totcount;
	int mergeuse;
	mergestore = new mergelist[100000];
	while(!queue.empty())
	{
		parent = queue.front();
		queue.pop_front();
		if(parent==NULL)
			continue;
		FreqPatternCount++;
		s2 = parent->nllength;
		row2 = parent->bufrow;
		col2 = parent->bufcol;
		lim2 = col2+3*s2;
		nlnode* tempnl; // stores last new node inserted as child to parent
		for(list<nlnode*>::iterator it=queue.begin();(*it)!=NULL;it++)
		{
			s1 = (*it)->nllength;
			row1 = (*it)->bufrow;
			col1 = (*it)->bufcol;
			lim1 = col1+3*s1;
			preptr=postptr=col2;
			postptr++;
			totcount = presum = postsum = mergeuse = 0;
			// Merge (*it) (ancestor) with parent (descendant)
			for(int i=col1;i<lim1;i+=3)
			{
				prelim = buffer[row1][i];
				postlim = buffer[row1][i+1];
				while(preptr<lim2 && buffer[row2][preptr]<=prelim)
				{
					presum += buffer[row2][preptr+2];
					preptr+=3;
				}
				while(postptr<lim2 && buffer[row2][postptr]<postlim)
				{
					postsum += buffer[row2][postptr+1];
					postptr+=3;
				}
				if(postsum>presum)
				{
					mergestore[mergeuse].pre = prelim;
					mergestore[mergeuse].post = postlim;
					mergestore[mergeuse].count = postsum-presum;
					mergeuse++;
					totcount += postsum-presum;
				}
			}
			if(totcount>=minsupport)
			{
				if(3*mergeuse+curbufcol>=bufsize)
				{
					bufendcol[curbufrow] = curbufcol;
					curbufrow+=1;
					buffer[curbufrow] = new int[bufsize];
					curbufcol=0;
				}
				if(parent->firstchild==NULL)
				{
					parent->firstchild = new nlnode;
					tempnl = parent->firstchild;
				}
				else
				{
					tempnl->nextsibling = new nlnode;
					tempnl = tempnl->nextsibling;
				}
				tempnl->name = (*it)->name;
				tempnl->support = totcount;
				tempnl->bufrow = curbufrow;
				tempnl->bufcol = curbufcol;
				tempnl->nllength = mergeuse;
				tempnl->firstchild = tempnl->nextsibling = NULL;
				for(int k=0;k<mergeuse;k++,curbufcol+=3)
				{
					buffer[curbufrow][curbufcol] = mergestore[k].pre;
					buffer[curbufrow][curbufcol+1] = mergestore[k].post;
					buffer[curbufrow][curbufcol+2] = mergestore[k].count;
				}
			}
		}
		tempnl = parent->firstchild;
		while(tempnl!=NULL)
		{
			queue.push_back(tempnl);
			tempnl=tempnl->nextsibling;
		}
		queue.push_back(NULL);
		/*
		if(lim2>=bufendcol[row2])
			delete buffer[row2];
		*/
	}
	cout << "Frequent patterns found = " << FreqPatternCount << endl;
}

int path[1000];
int pathsize=0;

void printpatterns(nlnode *root)
{
	// prints frequent itemsets along with their frequencies
	if(root->name>=0)
	{
		path[pathsize] = root->name;
		pathsize++;
		for(int i=pathsize-1;i>=0;i--)
			cout << freqitems[path[i]].name << " ";
		cout << "(" << root->support << ")\n";
	}
	nlnode *temp = root->firstchild;
	while(temp!=NULL)
	{
		printpatterns(temp);
		temp=temp->nextsibling;
	}
	if(root->name>=0)
		pathsize--;
}

void genkassoc(int cursup)
{
	// prints assoc rules with right side having only one item
	nlnode *temp;
	for(int i=0;i<pathsize;i++)
	{
		temp = nlroot;
		for(int j=0;j<pathsize;j++)
			if(i!=j)
				for(temp=temp->firstchild;temp!=NULL && temp->name!=path[j];temp=temp->nextsibling);
		for(int j=pathsize-1;j>=0;j--)
			if(i!=j)
				cout << freqitems[path[j]].name << " ";
		cout << "=> " << freqitems[path[i]].name << " ";
		cout << "( " << cursup << " / " <<  temp->support << " )\n";
	}
}

void genassocs(nlnode *root)
{
	if(root->name>=0)
	{
		path[pathsize] = root->name;
		pathsize++;
		if(pathsize>1)
			genkassoc(root->support);
	}
	nlnode *temp = root->firstchild;
	while(temp!=NULL)
	{
		genassocs(temp);
		temp=temp->nextsibling;
	}
	if(root->name>=0)
		pathsize--;
}

// TODO: make this pair count as a 2d array
map< pair<int,int> , int> pairCount;
void initPairCount(nlnode *root) {
	// prints frequent itemsets along with their frequencies
	if(root->name>=0) {
		//path[pathsize] = sorter[root->name];
		path[pathsize] = root->name;
		pathsize++;
		if(pathsize==2) {
			// TODO: change the constant based on dataset
			// TODO: check if confidence is a better metric here
			pairCount[make_pair(path[1],path[0])] = root->support;
			pairCount[make_pair(path[0],path[1])] = root->support;
		}
	}
	if(pathsize < 2) {
		nlnode *temp = root->firstchild;
		while(temp!=NULL) {
			initPairCount(temp);
			temp=temp->nextsibling;
		}
	}
	if(root->name>=0)
		pathsize--;
}

struct cluster
{
	int parent;
	int num_items;
	vector<int> child;
	cluster(){
		parent = -1;
		num_items = -1;
		child.clear();
	}
};

struct clust_dist {
	int clust1;
	int clust2;
	float dist;
};

cluster *clusters; // list of all clusters
int cluster_cnt = 0;   // number of clusters created till now
vector<int> cur_clusters; // clusters which are active for checking
bool *present; // stores if present or not

map<pair<int, int>, float> dist_clu;
float *dist_comp;

bool cmp(const clust_dist& lhs, const clust_dist& rhs) {
	if(lhs.dist < rhs.dist)
		return false;
	if(lhs.dist > rhs.dist)
		return true;
	if(lhs.clust1 < rhs.clust1)
		return false;
	if(lhs.clust1 > rhs.clust1)
		return true;
	if(lhs.clust2 < rhs.clust2)
		return false;
	if(lhs.clust2 > rhs.clust2)
		return true;
	return false;
}

priority_queue<int, vector<clust_dist>, decltype(&cmp) > min_heap(&cmp);

void print_heap() {

	while(!min_heap.empty()) {
		clust_dist temp = min_heap.top();
		cout << temp.clust1 << " " << temp.clust2 << " " << temp.dist << endl;
		min_heap.pop();
	}

	exit(0);
}

float link_distance(int cluster_A, int cluster_B) {
	if(cluster_A == cluster_B)
		return 0;
	if(dist_clu.find(make_pair(cluster_A, cluster_B)) != dist_clu.end()){
		return dist_clu[make_pair(cluster_A, cluster_B)];
	}
	if(dist_clu.find(make_pair(cluster_B, cluster_A)) != dist_clu.end()){
		return dist_clu[make_pair(cluster_B, cluster_A)];
	}
	// if(cluster_A == 6 && cluster_B == 5) {
	// 	cout << clusters[cluster_A].child.size() << endl;
	// 	cout << clusters[cluster_B].child.size() << endl;
	// }
	// average link
	float tot_distance = 0;
	float cur_distance;
	int child1_name;
	int child2_name;
	for(int child1 = 0; child1 < clusters[cluster_A].child.size(); child1++){
		for(int child2 = 0; child2 < clusters[cluster_B].child.size(); child2++){
			child1_name = clusters[cluster_A].child[child1];
			child2_name = clusters[cluster_B].child[child2];
			cur_distance = link_distance(child1_name, child2_name);
			// cur_distance *= clusters[child1_name].num_items;
			// cur_distance *= clusters[child1_name].num_items;
			tot_distance += cur_distance;
			// if(cluster_A == 70 && cluster_B == 2) {
			// 	cout << child1_name << " " << child2_name << " " << cur_distance <<  " " << tot_distance << " DEBUG" << endl;
			// 	// cout << link_distance << " DEBUG" << endl;
			// }
		}
	}
	// tot_distance /= clusters[cluster_A].num_items;
	// tot_distance /= clusters[cluster_B].num_items;
	dist_clu[make_pair(cluster_A, cluster_B)] = tot_distance;
	return tot_distance;
}

float standard_deviation = 0;
float beta = 0.5;

pair<int, int> close_clusters() {
	while(1) {
		clust_dist close_clust = min_heap.top();
		min_heap.pop();
		if(!(present[close_clust.clust1] && present[close_clust.clust2])) 
		{
			//cout << "DROPPED " << close_clust.clust1 << " " << close_clust.clust2 << endl;
			continue;
		}
		return make_pair(close_clust.clust1, close_clust.clust2);
	}
	// always return first two clusters
	//return make_pair(cur_clusters[0], cur_clusters[1]);
}

float get_distance_clu(pair<int,int>best_clusters) {
	float linkdistance = link_distance(best_clusters.first, best_clusters.second);
	linkdistance /= (float)(clusters[best_clusters.first].num_items);
	linkdistance /= (float)(clusters[best_clusters.second].num_items);
	return linkdistance;
}

float get_distance_comp(int cluster) {
	return dist_comp[cluster];
}

float get_disance_comp_avg(pair<int,int>best_clusters) {
	float comp_distance = dist_comp[best_clusters.first] + dist_comp[best_clusters.second];
	int comp1_pairs =  clusters[best_clusters.first].num_items;
	comp1_pairs = ((comp1_pairs + 1) * comp1_pairs)/2;
	int comp2_pairs =  clusters[best_clusters.second].num_items;
	comp2_pairs = ((comp2_pairs + 1) * comp2_pairs)/2;
	comp_distance /= (comp1_pairs + comp2_pairs);
}

float get_disance_comp_avg_single(int cluster) {
	int comp_pairs =  clusters[cluster].num_items;
	comp_pairs = ((comp_pairs + 1) * comp_pairs)/2;
	return dist_comp[cluster] / comp_pairs;
}

float get_standard_deviation() {
	return standard_deviation;
}

int decision(pair<int, int> best_clusters) {
	// always return merge
	int clusterA = best_clusters.first;
	int clusterB = best_clusters.second;


	float dist_clu = get_distance_clu(best_clusters);
	float dist_comp_avg = get_disance_comp_avg(best_clusters);
	float dist_comp_avg_A = get_disance_comp_avg_single(clusterA);
	float dist_comp_avg_B = get_disance_comp_avg_single(clusterB);

	cout << clusterA << " " << clusterB << endl;
	cout << dist_clu << " " << dist_comp_avg_A << " " << dist_comp_avg_B << " " << dist_comp_avg << endl;
	
	if(clusters[clusterA].num_items == 1 && clusters[clusterB].num_items == 1 )
		return 0;

	if(clusters[clusterA].num_items == 1 || clusters[clusterB].num_items == 1 )
		return 2;

	float sigma = get_standard_deviation();

	if(dist_clu - dist_comp_avg > sigma) {
		return 0;	//merge A,B
	}
	else if(abs(dist_comp_avg_A - dist_comp_avg_B) < beta * dist_comp_avg) {
		return 1;	// sub-clusters of A,B are merged
	}
	else if(dist_comp_avg_A > dist_comp_avg_B) {
		return 2;	// B is subcluster of A
	}
	else {
		return 3;	// A is subcluster of B
	}
}


void delete_cluster(int cluster){
    auto pr = std::equal_range(cur_clusters.begin(), cur_clusters.end(), cluster);
    cur_clusters.erase(pr.first, pr.second);
    present[cluster] = false;
}

// decision - 0 merge
void combine_clusters(pair<int,int> best_clusters, int decision) {
	cout << "combined " << best_clusters.first << " , " << best_clusters.second << " with decision " << decision<< " to form " << cluster_cnt << endl;
	int clust1 = best_clusters.first;
	int clust2 = best_clusters.second;
	if(decision == 0) {
		clusters[cluster_cnt].child.push_back(clust1);
		clusters[cluster_cnt].child.push_back(clust2);
		clusters[clust1].parent = cluster_cnt;
		clusters[clust2].parent = cluster_cnt;
		dist_comp[cluster_cnt] = get_distance_clu(best_clusters);
	}
	else if(decision == 1) {
		for(int child = 0; child < clusters[clust1].child.size(); child ++)
		{
			clusters[cluster_cnt].child.push_back(clusters[clust1].child[child]);
			clusters[clusters[clust1].child[child]].parent = cluster_cnt;
		}
		for(int child = 0; child < clusters[clust2].child.size(); child ++)
		{
			clusters[cluster_cnt].child.push_back(clusters[clust2].child[child]);
			clusters[clusters[clust2].child[child]].parent = cluster_cnt;
		}
		dist_comp[cluster_cnt] = dist_comp[clust1] + dist_comp[clust2];
		dist_comp[cluster_cnt] += get_distance_clu(best_clusters);
		// for(int child1 = 0; child1 < clusters[clust1].child.size(); child1 ++){
		// 	for(int child2 = 0; child2 < clusters[clust2].child.size(); child2 ++){
		// 		dist_comp[cluster_cnt] += get_distance_clu(make_pair(clusters[clust1].child[child1], clusters[clust2].child[child2]));
		// 	}
		// }
	}
	else if(decision == 2) {
		for(int child = 0; child < clusters[clust1].child.size(); child ++)
		{
			clusters[cluster_cnt].child.push_back(clusters[clust1].child[child]);
			clusters[clusters[clust1].child[child]].parent = cluster_cnt;
		}
		clusters[cluster_cnt].child.push_back(clust2);
		clusters[clust2].parent = cluster_cnt;
		dist_comp[cluster_cnt] = dist_comp[clust1];
		dist_comp[cluster_cnt] += get_distance_clu(best_clusters);
		// for(int child1 = 0; child1 < clusters[clust1].child.size(); child1 ++){
		// 	dist_comp[cluster_cnt] += get_distance_clu(make_pair(clusters[clust1].child[child1], clust2));
		// }
	}
	else {
		for(int child = 0; child < clusters[clust2].child.size(); child ++)
		{
			clusters[cluster_cnt].child.push_back(clusters[clust2].child[child]);
			clusters[clusters[clust2].child[child]].parent = cluster_cnt;
		}
		clusters[cluster_cnt].child.push_back(clust1);
		clusters[clust1].parent = cluster_cnt;
		dist_comp[cluster_cnt] = dist_comp[clust2];
		dist_comp[cluster_cnt] += get_distance_clu(best_clusters);
		// for(int child2 = 0; child2 < clusters[clust2].child.size(); child2 ++){
		// 	dist_comp[cluster_cnt] += get_distance_clu(make_pair(clusters[clust2].child[child2], clust1));
		// }
	}
	clusters[cluster_cnt].num_items = clusters[clust1].num_items + clusters[clust2].num_items;
	delete_cluster(clust1);
	delete_cluster(clust2);
	present[cluster_cnt] = true;
	for(int i=0;i<cur_clusters.size();i++) {
		clust_dist temp = clust_dist();
		temp.clust1 = cluster_cnt;
		temp.clust2 = cur_clusters[i];
		temp.dist = get_distance_clu(make_pair(cluster_cnt, cur_clusters[i]));
		min_heap.push(temp);
	}
	cur_clusters.push_back(cluster_cnt);
	cluster_cnt ++;
}

void dfs(int parent) {
	for(int i = 0;i<clusters[parent].child.size();i++) {
		int child = clusters[parent].child[i];
		if(child != parent){
			cout << parent << " -> " << child << endl;
			dfs(child);
		}
	}
}


void hierarchical_clsutering() {
	pathsize = 0;
	initPairCount(nlroot);
	clusters = (cluster*) malloc(freqcnt * 3 * sizeof(cluster));
	cluster_cnt = 0;
	cur_clusters.clear();
	present = (bool*) malloc(freqcnt * 3 * sizeof(bool));
	
	dist_clu.clear();
	dist_comp = (float*) malloc(freqcnt * 3 * sizeof(float));

	while(!min_heap.empty())
		min_heap.pop();
	
	for(int i = 0;i < freqcnt; i++) {
		clusters[i].num_items = 1;
		clusters[i].child.push_back(i);
		cur_clusters.push_back(i);
		present[i] = true;
		dist_comp[i] = 0;
	}
	cluster_cnt = freqcnt;
	
	float square_sum = 0;
	float linear_sum = 0;
	float n = freqcnt * freqcnt;
	for(int i = 0;i < freqcnt; i++) {
		for(int j = i + 1;j < freqcnt;j++) {
			dist_clu[make_pair(i, j)] = 100000.0/(pairCount[make_pair(i,j)] + 1.0);
			// dist_clu[make_pair(i, j)] = (freqitems[i].freq + freqitems[j].freq)/(float)(pairCount[make_pair(i,j)] + 1.0);
			float dis = dist_clu[make_pair(i, j)];
			linear_sum += 2.0 * dis;
			square_sum += 2.0 * dis * dis;
			clust_dist temp = clust_dist();
			temp.clust1 = i;
			temp.clust2 = j;
			temp.dist = get_distance_clu(make_pair(i, j));
			min_heap.push(temp);
		}
	}

	standard_deviation = square_sum / n;
	standard_deviation -= (linear_sum/n) * (linear_sum/n);
	standard_deviation = pow(standard_deviation, 0.5);
	// cout << standard_deviation << " SD" << endl;

	// while(!min_heap.empty()) {
	// 	pair<int, int> close_clust = close_clusters();
	// 	cout << close_clust.first << " " << close_clust.second << endl;
	// }
	while(cur_clusters.size() != 1) {
		pair<int,int> best_clusters = close_clusters();
		int combine_decision = decision(best_clusters);
		combine_clusters(best_clusters, combine_decision);
		//print_heap();
	}
	cout << "digraph G {" << endl;
	dfs(cur_clusters[0]);
	cout << "}" << endl;
}

void dfs_path(int parent, ofstream& new_h, vector<int>& path) {
	path.push_back(parent);
	if(clusters[parent].child.empty() || (clusters[parent].child.size()==1 && clusters[parent].child[0]==parent)) {
		int last = path.size();
		for(int i= 0 ; i< last; i++) {
			if(i==last-1) {
				new_h << freqitems[path[i]].name << endl;
			}
			else
				new_h << path[i] << "|";	
		}
	}
	for(int i = 0;i<clusters[parent].child.size();i++) {
		int child = clusters[parent].child[i];
		if(child != parent){
			//cout << parent << " -> " << child << endl;
			dfs_path(child, new_h, path);
		}
	}
	path.pop_back();
}

int main(int argc,char **argv)
{
	/*
	char filename[] = "../../Datasets/pumsb.dat";
	double support = 0.8;
	*/
	char* filename = argv[1];
	double support = atof(argv[2]);
	// Initialize root of fp tree
	ppcroot.label = -1;
	ppcroot.precode = ppcroot.postcode = ppcroot.frequency = -1;
	ppcroot.firstchild = ppcroot.nextsibling = NULL;
	// Read data and store counts of items
	read_data(filename,support);
	// Build FP tree to store the required part in compressed form
	build_ppc(filename);
	int c=0;
	for(int k=0;k<freqcnt;k++)
		c+=headlen[k];
	cout << "Header count = " << c << endl;
	// create node list tree with sigletons as children of root
	initialize_nltree();
	generate();
	//printpatterns(nlroot);
	hierarchical_clsutering();
	//cout << "Beginning" << endl;
	//constructCH();
	//genassocs(nlroot);
	ofstream new_h("new_hierarchy.dat");
	vector<int> path;
	path.clear();
	dfs_path(cur_clusters[0], new_h, path);
	return 0;
}