#include<bits/stdc++.h>

using namespace std;

//std::ifstream dataset( "Datasets/ml-100k/u1.base" );
std::ifstream dataset( "Datasets/ml-100k/u.data" );
std::ofstream movies( "Datasets/movies1" );


map<string, int> mapping;
string db[944][510]; //Entire dastaset of size No.of users * max no.of ratings by a user
int lendb[944];  //Lengths of each transactions

std::vector<std::string> split(const std::string& s, char c)
{
    std::vector<std::string> v;
    unsigned int i = 0;
    unsigned int j = s.find(c);
    while (j < s.length()) {
        v.push_back(s.substr(i, j - i));
        i = ++j;
        j = s.find(c, j);
        if (j >= s.length()) {
            v.push_back(s.substr(i, s.length()));
            break;
        }
    }
    return v;
}

void build_data()
{
    for( std::string line; getline( dataset, line ); )
    {
        vector<string> words=split(line,'\t');
        /*for(int i=0;i<words.size();i++)
            cout << words[i] << " ";
        cout << "\n";*/
        if(atoi(words[2].c_str())>2)
        {
            int id=atoi(words[0].c_str());
            int l=lendb[id];
            db[id][l]=words[1];
            lendb[id]++;
        }
    }
    // To print the trasactions
    for(int i=1;i<944;i++)
    {
        for(int j=0;j<lendb[i];j++)
        {
            movies << db[i][j] << " ";
        }
        movies << "\n";
    }
    /*for(int i=1;i<1684;i++)
    {
        cout << i << " ";
        for(int j=0;j<4;j++)
            cout << parents[i][j] << " ";
        cout << "\n";
    }*/
}

int main()
{
    build_data();
    return 0;
}
