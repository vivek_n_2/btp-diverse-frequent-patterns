#include <bits/stdc++.h>

using namespace std;

#define MAX_ITEM 100000

typedef vector< vector<int> > vec2D;

pair<int, int>* counter;

unordered_map<int, pair<int, int> > item_rank;

int min_count;

int freq_count;

int transaction_count;

int** pair_count;

void read_data(char *file_name, double items_percentage)
{
	ifstream infile(file_name);	

	transaction_count = 0;
	int item_count = 0;
	int max_item = 0;

	// Allocate memory for counter and initialize to 0
	counter = new pair<int, int>[MAX_ITEM];
	for(int i=0;i<MAX_ITEM;i++) {
		counter[i].first = 0;
		counter[i].second = 0;
	}
	
	// Read input file line by line and count items
 	string transaction;
	int item;
	while(getline(infile, transaction))
	{
		item = 0;
		transaction_count++;
		for(int i=0;i<transaction.length();i++)
		{
			if(transaction[i] >= '0' && transaction[i] <= '9') {
				item = item*10 + transaction[i]-'0';
			} else if(item > 0) {
				max_item = max(max_item, item);
				if(counter[item].first == 0) {
					item_count++;
					counter[item].second = item_count;
				}
				counter[item].first++;
				item = 0;
			}
		}
	}

	double min_support = items_percentage;
	min_count = ceil(transaction_count * min_support);

	sort(counter + 1, counter + item_count + 1, greater< pair<int, int> >());

	freq_count = item_count;
	for(int i=1;i<=item_count;i++){
		if(counter[i].first < min_count) {
			freq_count = i-1;
			break;
		}
		item_rank[counter[i].second] = make_pair(i,counter[i].first);
	}

	cout << "transaction_count: " << transaction_count << " , item_count: " << item_count;
	cout << " , min_count: " << min_count << endl;
	cout << "freq_count: " << freq_count << endl;
}

void init_pair_count(char *file_name) {
	pair_count = (int **) malloc((freq_count+1) * sizeof(int *));
	for(int i=0;i<=freq_count;i++) {
		pair_count[i] = (int *) malloc((freq_count+1) * sizeof(int));
		for(int j=0;j<=freq_count;j++)
			pair_count[i][j] = 0;
	}

	ifstream infile(file_name);	
	
	// Read input file line by line and count items
 	string transaction;
	int item;
	int* transaction_list = (int *)malloc(MAX_ITEM*sizeof(int));
	int transaction_len = 0;
	while(getline(infile, transaction))
	{
		item = 0;
		transaction_len = 0;
		for(int i=0;i<transaction.length();i++)
		{
			if(transaction[i] >= '0' && transaction[i] <= '9') {
				item = item*10 + transaction[i]-'0';
			} else if(item > 0) {
				transaction_list[transaction_len++] = item;
				item = 0;
			}
		}

		int item1, item2;
		int count1, count2;
		for(int i=0;i<transaction_len;i++) {
			item1 = item_rank[transaction_list[i]].first;
			count1 = item_rank[transaction_list[i]].second;
			if(count1 < min_count)
				continue;
			for(int j=i;j<transaction_len;j++) {
				item2 = item_rank[transaction_list[j]].first;
				count2 = item_rank[transaction_list[j]].second;
				if(count2 < min_count)
					continue;

				if(item1 < item2)
					pair_count[item1][item2]++;
				else
					pair_count[item2][item1]++;
			}
		}
	}

	//cout << pair_count[1][1] << endl;
}


/********************************************************************/
int* visited;
vec2D adj;

// u, v are indexes and not the names
// support based
bool isEdge(int u, int v,int edgeSup) {
	return pair_count[u][v]>=edgeSup;
}

// confidence based
bool isEdge2(int u, int v,double edgeSup) {
	return pair_count[u][v]>=ceil(edgeSup*max(pair_count[u][u], pair_count[v][v]));
}

void buildAdj(vector<int>& nodes, double edgeSupFloat) {
	int edgeSup = ceil(edgeSupFloat*transaction_count);
	int nodeCount = nodes.size();
	for(int idx = 0;idx < nodeCount;idx++) {
		adj[nodes[idx]].clear();
	}
	for(int idx = 0;idx < nodeCount;idx++) {
		int u = nodes[idx];
		visited[u] = 0;
		for(int idx2 = idx+1;idx2 < nodeCount;idx2++) {
			int v = nodes[idx2];
			if(isEdge(u,v,edgeSup)) {
			//if(isEdge2(u,v,edgeSupFloat)) {
				adj[u].push_back(v);
				adj[v].push_back(u);
				//cout << counter[u].second << " edged with " << counter[v].second <<  endl;
			}
		}
	}
}

void connectedComponents(int parent, int compId){
	if(visited[parent])
		return;
	visited[parent] = compId;
	int childCount = adj[parent].size();
	for(int idx=0;idx<childCount;idx++) {
		int child = adj[parent][idx];
		connectedComponents(child, compId);
	}
}

int connectedComponentsUtil(vector<int>& nodes) {
	int nodeCount = nodes.size();
	int compCount = 0;
	for(int idx = 0;idx < nodeCount;idx++) {
		int u = nodes[idx];
		if(!visited[u]) {
			compCount ++;
			connectedComponents(u, compCount);
		}
	}
	return compCount;
}


double EPS = 0.0001;
void recurseCH(vector<int>& nodes) {

	double left = 0;
	double right = 1.0;
	double mid;
	int connectedComps;
	while(right-left>EPS)
	{
		mid = (left+right)/2.0;
		buildAdj(nodes, mid);
		connectedComps = connectedComponentsUtil(nodes);
		if(connectedComps < 5){
			left = mid;
		}
		else
			right = mid;
	}
	//mid = 1;
	mid = 0.005769;
	cout << "Threshold = " << mid << endl;
	buildAdj(nodes, mid);
	int comp_count = connectedComponentsUtil(nodes);
	int* comp_counter = new int[comp_count+1];
	for(int idx=0;idx<=comp_count;idx++){
		comp_counter[idx] = 0;
	}
	int node_count = nodes.size();
	for(int idx = 0;idx < node_count;idx++) {
		//cout << freqitems[nodes[idx]].name << " " << visited[nodes[idx]] << " comp name " << endl;
		comp_counter[visited[nodes[idx]]]++;
	}
	
	for(int idx=1;idx<=comp_count;idx++){
		cout << comp_counter[idx] << " ";
	}
	
	cout << endl;
}

void constructCH() {
	visited = (int*) malloc((freq_count + 1)*sizeof(int));
	adj.clear();
	adj.resize(freq_count + 1);
	for(int i=0;i<=freq_count;i++) {
		visited[i] = 0;
		adj[i].clear();
	}
	
	vector<int> nodes;
	nodes.clear();
	for(int i=1;i<=freq_count;i++) {
		nodes.push_back(i);
	}
	recurseCH(nodes);
	//connectedcomps
}
/********************************************************************/

int main(int argc, char* argv[]) {

	if(argc != 3) {
		cerr << "Usage: " << argv[0] << " <file_name> <min_support>" << endl;
		exit(0);
	}

	char* file_name = argv[1];
	double min_support = atof(argv[2]);

	cout << "file_name: " << file_name << " , min_support: " << min_support << endl;

	read_data(file_name, min_support);

	init_pair_count(file_name);

	constructCH();
}